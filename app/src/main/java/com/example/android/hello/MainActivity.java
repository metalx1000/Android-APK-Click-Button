package com.filmsbykris.hello;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.util.Log;

public class MainActivity extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    Button button_1 = (Button) findViewById(R.id.button_1);
    Button button_2 = (Button) findViewById(R.id.button_2);
    button_1.setText("CLICK ME");


    button_1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view)
      {
        // Do something
        TextView text_1 = findViewById(R.id.text_1);
        text_1.setText("Good Evening!");
        Log.e("Service", "Service is running from FilmsByKris...");
      }
    });

    button_2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view)
      {
        // Do something
        TextView text_2 = findViewById(R.id.text_2);
        text_2.setText("Click Click CLick!");
        Log.e("Service", "Service is running from FilmsByKris...");
      }
    });
  }



}
